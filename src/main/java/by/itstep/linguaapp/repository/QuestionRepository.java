package by.itstep.linguaapp.repository;

import by.itstep.linguaapp.entity.QuestionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestionRepository
        extends JpaRepository<QuestionEntity, Integer> {
}
