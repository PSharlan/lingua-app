package by.itstep.linguaapp.repository;

import by.itstep.linguaapp.entity.UserEntity;
import by.itstep.linguaapp.entity.UserRole;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@SpringBootTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    @Transactional // import org.springframework.transaction.annotation.Transactional;
    public void testDeleteAdmins() {
        // given

        // when
        userRepository.deleteAllAdmins();
        userRepository.flush();

        // then
    }

}
